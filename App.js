import * as React from 'react';
import { Text, View, StyleSheet, Button, Image} from 'react-native';
import { Audio, Video, AVPlaybackStatus } from 'expo-av';
import * as DocumentPicker from 'expo-document-picker';
// import SoundPlayer from "react-native-sound"
// import Sound from "react-native-sound"



export default function App() {

//   const player = useAudioHelper({
//     listSounds: [
//         { type: 'app-bundle', path: 'Daft Punk - Robot Rock (Official Video).mp3', name: 'Robot Rock', basePath: SoundPlayer.MAIN_BUNDLE },
//         { type: 'network', path: 'https://raw.githubusercontent.com/uit2712/RNPlaySound/develop/sounds/Tropic%20-%20Anno%20Domini%20Beats.mp3', name: 'Tropic - Anno Domini Beats', },
//     ],
//     timeRate: 15,
//     isLogStatus: true,
// });

// const [music,setMusic]=useState("")

// const play=()=>{
//   let summer =new Sound("DaftPunk.mp3",Sound.MAIN_BUNDLE,(err)=>{
//     if(err){
//       console.log("error",err)
//     }
//     summer.play((success)=>{
//       console/log("end",success)
//     })
//   })
// }


  const [sound, setSound] = React.useState();


  async function playSound() {
    console.log('Loading Sound');
    if(fileAudio){
      debugger
      const { sound } = await Audio.Sound.createAsync(
        file? 'file://'+file.uri:'./assets/Tiptoe.mp3'
      );
      setSound(sound);
    }

    console.log('Playing Sound');
    await sound.playAsync();
  }

  // React.useEffect(() => {
  //   setupPlayer()
  // }, []);

  React.useEffect(() => {
    return sound
      ? () => {
        console.log('Unloading Sound');
        sound.unloadAsync();
      }
      : undefined;
  }, [sound]);

  //////////////////////

  const [recording, setRecording] = React.useState();

  async function startRecording() {
    try {
      console.log('Requesting permissions..');
      await Audio.requestPermissionsAsync();
      await Audio.setAudioModeAsync({
        allowsRecordingIOS: true,
        playsInSilentModeIOS: true,
      });
      console.log('Starting recording..');
      const { recording } = await Audio.Recording.createAsync(
        Audio.RECORDING_OPTIONS_PRESET_HIGH_QUALITY
      );
      setRecording(recording);
      console.log('Recording started');
    } catch (err) {
      console.error('Failed to start recording', err);
    }
  }

  async function stopRecording() {
    console.log('Stopping recording..');
    setRecording(undefined);
    await recording.stopAndUnloadAsync();
    const uri = recording.getURI();
    console.log('Recording stopped and stored at', uri);
  }

  ////////////////////

  const video = React.useRef(null);
  const [status, setStatus] = React.useState({});

  ////////////////////

  const [file, setFile] = React.useState()
  const [fileVideo,setFileVideo]=React.useState()
  const [fileAudio,setFileAudio]=React.useState()

  const pickDocument = async () => {
    let result = await DocumentPicker.getDocumentAsync({});
    let a=result.name.split('.')
    console.log(a[a.length-1])
    debugger
    if(a[a.length-1]=="jpg"){
      setFile(result)
    }else if(a[a.length-1]=="mp4"){
      setFileVideo(result)
    }else if(a[a.length-1]=="mp3"){
      setFileAudio(result)
    }
    
    else{
      console.log("false",a);
    }

  };

  return (
    <View style={styles.container}>
      <Button title="Play Sound" onPress={playSound} />
      <Button
        title={recording ? 'Stop Recording' : 'Start Recording'}
        onPress={recording ? stopRecording : startRecording}
      />
      <View>
        <Video
          ref={video}
          style={styles.video} fileVideo
          source={{
            uri: fileVideo? 'file://'+fileVideo.uri:'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4'
          }}
          useNativeControls
          resizeMode="contain"
          isLooping
          onPlaybackStatusUpdate={status => setStatus(() => status)}
        />
        <View style={styles.buttons}>
          <Button
            title={status.isPlaying ? 'Pause' : 'Play'}
            onPress={() =>
              status.isPlaying ? video.current.pauseAsync() : video.current.playAsync()
            }
          />
        </View>
      </View>
      <View>
        <Button
          title="Upload file"
          onPress={pickDocument}
        />
      </View>

      <View>
        <Button
          title="Music"
          onPress={()=>{play()}}
        />
      </View>

      <Image
        source={{
          uri: file? 'file://'+file.uri:null
        }}
        style={{ width: 200, height: 200, borderRadius: 10 }}
      />



    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
    padding: 10,
  },
  video: {
    alignSelf: 'center',
    width: 320,
    height: 200,
  },
  buttons: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});


