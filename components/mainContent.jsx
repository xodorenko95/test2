import React, { useState } from 'react';
import { StyleSheet, Text, View, Image, TextInput } from 'react-native';

const MainContent = () => {

    const [state, setState] = useState("")

    return (
        <View style={styles.container}>
            <Image
                source={{
                    uri: "https://www.spletnik.ru/img/__post/60/60cf0ac41d8c3404a26fb544a0887c1d_994.jpg"
                }}
                style={{ width: 200, height: 200, borderRadius: 10 }}
            />
            <Text>Press Here</Text>

            <TextInput onChangeText={text => setState(text)}
                defaultValue={state} placeholder="Write here"
                keyboardType="twitter"
            />
            <Text>{state}</Text>
        </View>
    );
}

export default MainContent;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
